<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\CheckTaskExpiration::class,
        Commands\DeleteExpiredTask::class,
        Commands\NotifyExpiringTask::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // command to mark a task expired
        $schedule->command('mark:expire')
            ->dailyAt('00:00');

        // command to check if the task hass been expired than delete the task permanently
        $schedule->command('delete:expiredtask')
            ->monthly();

        // command to notify the user that task is going to expire
        $schedule->command('expiring:task')
            ->dailyAt('09:00');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
