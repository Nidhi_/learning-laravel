<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Todo\Task\Task;
use App\Todo\Task\TaskApi;
use Illuminate\Support\Facades\Log;
use App\Todo\TodoPayload;
use App\Todo\Notification\NotificationApi;

class CheckTaskExpiration extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mark:expire';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'To check if the task has not been completed and the completion date has been emerged then mark the task expire.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {   
        Task::query()->expired()->each(function($task){
            TaskApi::doMarkExpire($task);

            $title = $task->title.' task has been expired.';
            $type = 'Task Expired';
            $user_id = $task->user_id;

            $payload = new TodoPayload([
                'user_id' => $user_id,
                'type' => $type,
                'title' => $title
            ]);

            NotificationApi::doCreate($payload);
            Log::info($task->id. 'Expired');
        });
    }
}
