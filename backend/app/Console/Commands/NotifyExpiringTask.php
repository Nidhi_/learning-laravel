<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Todo\Task\Task;
use Illuminate\Support\Facades\Log;
use App\Events\ExpiringTask;
use App\Todo\Notification\NotificationApi;
use App\Todo\TodoPayload;

class NotifyExpiringTask extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'expiring:task';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notify Task that are going to be expired';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // it will get all the task that has been marked expired
        Task::query()->getExpiringTask()->each(function($task){

            // an event is dispatched to notify users that the task is going to expire
            $title = $task->title. ' task is going to get expired today!!';
            $type = 'Task Expiring';

            event(new ExpiringTask($task, $title));
            
            $payload = new TodoPayload([
                'user_id' => $task->user_id,
                'type' => $type,
                'title' => $title
            ]);
            NotificationApi::doCreate($payload);
            Log::info($title);
        });
    }
}
