<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Todo\Task\Task;
use App\Todo\Task\TaskApi;
use Illuminate\Support\Facades\Log;
use App\Events\TaskDeleted;
use App\Todo\TodoPayload;
use App\Todo\Notification\NotificationApi;

class DeleteExpiredTask extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'delete:expiredtask';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'To check if the task has been marked expired, than delete that task permanently';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // it will get all the task that has been marked expired
        Task::query()->getExpiredTask()->each(function($task){
            
            // delete the expired task permanently
            TaskApi::permanentDelete($task);

            $title = $task->title.' task has been permanently deleted.';
            $type = 'Task Deleted Permanently';
            $user_id = $task->user_id;

            $payload = new TodoPayload([
                'user_id' => $user_id,
                'type' => $type,
                'title' => $title
            ]);

            NotificationApi::doCreate($payload);
            Log::info($task->id. 'Permanently Deleted');
        });

        // an event is dispatched
        $title = 'All one month old expired tasks has been permanently deleted.';
        event(new TaskDeleted($title));
    }
}
