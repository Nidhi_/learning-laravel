<?php
namespace App\Todo;

use Exception;
use App\Todo\TodoException;
use Illuminate\Support\Facades\Validator;

class TodoValidation
{
    /**
     * validate fields of given data with given rules
     */
    public function validate(array $fields, $data)
    {
        $applicable_rules = array();
        foreach ($fields as $field) {
            $applicable_rules[$field] = $this->rules[$field] ?? [];
        }
        if (count($applicable_rules)) {
            $this->doValidateRules($data, $applicable_rules);
        }
    }

    protected function doValidateRules($data, $rules)
    {
        $validator = Validator::make((array) $data, $rules);
        if ($validator->fails()) {
            $e = new TodoException($validator->errors()->first());
            throw $e;
        }
    }
}
