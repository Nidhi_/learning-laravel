<?php
namespace App\Todo;

use App\Todo\Session\Session;
use App\Todo\User\User;

class TodoApi
{
    static $session = null;
    
    public static function setLoggedInSession(Session $session) 
    {
        self::$session = $session;
    }

    public static function getLoggedInUser()
    {
        if(!self::$session) {
            throw new TodoException(
                'No Session set for user.',
                419
            );
        }
        $user_id = (self::$session)->user_id;
        return User::whereId($user_id)->firstOrFail();
    }
}
