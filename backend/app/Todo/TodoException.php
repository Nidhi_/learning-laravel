<?php

namespace App\Todo;

use Exception;

class TodoException extends Exception
{
    public $code = 0;
    public $status = 500;
    public $message = '';
    public $title = 'Error !';
    public $details = '';

}
