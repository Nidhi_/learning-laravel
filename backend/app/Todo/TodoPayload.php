<?php
namespace App\Todo;

use Illuminate\Http\Request;

class TodoPayload
{
    public function __construct($data)
    {
        if ($data) {
            foreach ($data as $key => $value) {
                $this->{$key} = $value;
            }
        }
        return $this;
    }

    public function _appendClientPlatformIP(Request $request)
    {
        $this->ip_address = $request->getClientIp();
        $this->platform_signature = $request->header('User-Agent');
    }

    // TODO: Implement
    // public function toArray()
    // {

    // }
}
