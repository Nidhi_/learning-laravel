<?php

namespace App\Todo\Session;

use App\Todo\TodoPayload;
use App\Todo\TodoApi;
use App\Todo\TodoException;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Todo\User\User;

class Session extends Model
{
    use SoftDeletes;
    protected $table = 'sessions';
    protected $fillable = ['key','user_id','otp','ip_address','last_active_at','secret'];
    protected $hidden = ['otp','secret'];

    public $incrementing = false;

    /**
    * The primary key for the model.
    *
    * @var string
    */
    protected $primaryKey = 'key';

    /**
    * RELATION SHIPS
    */

    public function user()
    {
      return $this->belongsTo('App\Todo\User\User');
    }

    /**
    * SCOPES
    */

    /**
    * FUNCTIONS
    */

    public function authorize(TodoPayload $payload){
        $to_be_hashed = $payload->key . $this->secret . $payload->timestamp;
        $expected_hash = hash('sha256', $to_be_hashed);
        if($payload->hash != $expected_hash) {
            throw new TodoException(
                'Something went wrong. Please login again.',
                419
            );
        }

        // update last_active time of user
        $this->last_active_at = now();
        $this->save();

        // set current session & user
        $this->setLoggedInSession();

        return $this;
    }

    public function setLoggedInSession() {
        TodoApi::setLoggedInSession($this);
    }

    public function doCreate(User $user, TodoPayload $payload)
    {
        (new SessionValidation($payload))->validateCreate();

        $payload->key = (string) Str::uuid();
        $payload->user_id = $user->id;
        $payload->otp = generateOtp();
        $this->fill((array) $payload)->save();
        return $this;
    }

    public function verifyOtp(TodoPayload $payload)
    {
        // (new SessionValidation($payload))->validateVerifyOtp();

        if(doHash($this->otp) == doHash($payload->otp)){
            $payload->secret = empty($this->secret)
                ? crypt((string) Str::uuid(), mt_rand())
                : $this->secret;
            $payload->otp = null;
            $payload->last_active_at = now();
            $this->fill((array) $payload)->save();

            return $this;
        }
        else {
            throw new TodoException('OTP is invalid');
        }
    }
}
