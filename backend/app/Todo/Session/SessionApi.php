<?php

namespace App\Todo\Session;

use App\Todo\User\User;
use App\Todo\TodoApi;
use App\Todo\TodoPayload;
use App\Todo\Session\Session;

class SessionApi extends TodoApi
{
    // get session
    static function getByKey($key) {
        return Session::whereKey($key)->firstOrFail();
    }

    // user token
    static function authorizeByToken($token) {
        $decoded_token = json_decode($token);

        // to get an instance
        $payload = new TodoPayload($decoded_token);
        $session = self::getByKey($payload->key);
        return $session->authorize($payload);
    }

    static function create(TodoPayload $payload, $user)
    {
        return (new Session())->doCreate($user, $payload);
    }

    static function verifyOtp($key, TodoPayload $payload)
    {
        return Session::whereKey($key)->firstOrFail()->verifyOtp($payload);
    }

    static function logout($key)
    {
        // we can use email also
        Session::whereKey($key)->firstOrFail()->delete();
    }
}
