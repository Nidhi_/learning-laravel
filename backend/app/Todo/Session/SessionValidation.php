<?php

namespace App\Todo\Session;

use App\Todo\TodoValidation;
use App\Todo\TodoPayload;
use Illuminate\Foundation\Http\FormRequest;

class SessionValidation extends TodoValidation
{
    const KEY = 'key';
    const SECRET = 'secret';
    const EMAIL = 'email';
    const USER_ID = 'user_id';
    const OTP = 'otp';
    const IP_ADDRESS = 'ip_address';

    public function __construct(TodoPayload $data)
    {
        $this->data = $data;

        $this->rules = [
            self::KEY => 'required|string|exists:sessions,key',
            self::USER_ID => 'required|numeric|exists:users,id',
            self::IP_ADDRESS => 'required|string',
            self::EMAIL => 'required|email|exists:users,email',
            self::OTP => 'required|numeric'
        ];
    }

    /**
     * validate given data  for create action
     */
    public function validateCreate()
    {
        $fields = [
            self::EMAIL
        ];

        $this->validate($fields, $this->data);
    }

    /**
     * validate given data for verify otp action
     */
    public function validateVerifyOtp()
    {
        $fields = [
            self::KEY,
            self::OTP
        ];

        $this->validate($fields, $this->data);
    }

    /*
     * validate given data for LOGOUT action
     */
    public function validateLogout()
    {
        $fields = [
            self::KEY
        ];
        
        $this->validate($fields, $this->data);
    }
}
