<?php

namespace App\Todo\Task;

use App\Todo\TodoValidation;
use Illuminate\Foundation\Http\FormRequest;

class TaskValidation extends TodoValidation
{

    /**
     * Define all column names which are going to be part of validation
     * as a constant.
     */
    const ID = 'id';
    const TITLE = 'title';
    const DESCRIPTION = 'description';
    const COMPLETION_DATE = 'completion_date';

    public function __construct($data)
    {
        $this->data = $data;

        $this->rules = [
            self::ID => 'required|exists:tasks',
            self::TITLE => 'bail|required|min:2|max:15',
            self::DESCRIPTION => 'nullable|max:50',
            self::COMPLETION_DATE => 'bail|required|after:yesterday'
        ];
    }

    public function validateCreate() {
        $fields = [
            self::TITLE,
            self::COMPLETION_DATE
        ];

        $this->validate($fields, $this->data);
    }

    public function validateUpdate() {
        $fields = [
            self::ID,
            self::TITLE,
            self::COMPLETION_DATE
        ];

        $this->validate($fields, $this->data);
    }
}
