<?php

namespace App\Todo\Task;

use App\Todo\TodoApi;
use App\Todo\TodoPayload;

class TaskApi extends TodoApi
{


    // function to get list of users

    public static function getList(TodoPayload $payload)
    {
        return (new Task())->getList($payload);
    }

    public static function doCreate(TodoPayload $payload)
    {
        return (new Task())->doCreate($payload);
    }

    public static function getInstance(TodoPayload $payload, $id)
    {
        return (new Task())->getInstance($payload, $id);
    }

    public static function doUpdate(TodoPayload $payload, $id)
    {
        return (new Task())->doUpdate($payload, $id);
    }

    public static function doDelete(TodoPayload $payload, $id)
    {
        return (new Task())->doDelete($payload, $id);
    }

    public static function doDeleteMultiple(TodoPayload $payload)
    {
        return (new Task())->doDeleteMultiple($payload);
    }

    public static function getCompletedTasks(TodoPayload $payload)
    {
        return (new Task())->getCompletedTasks($payload);
    }

    public static function getPendingTasks(TodoPayload $payload)
    {
        return (new Task())->getPendingTasks($payload);
    }

    public static function getDeletedTasks(TodoPayload $payload)
    {
        return (new Task())->getDeletedTasks($payload);
    }

    public static function doRestore(TodoPayload $payload, $id)
    {
        return (new Task())->doRestore($payload, $id);
    }

    public static function doPermanentDelete(TodoPayload $payload)
    {
        return (new Task())->doPermanentDelete($payload);
    }

    public static function doMarkExpire($task)
    {
        return ($task->doMarkExpire());
    }

    public static function getExpiredTasks(TodoPayload $payload)
    {
        return (new Task())->getExpiredTasks($payload);
    }

    public static function permanentDelete($task)
    {
        return ($task->permanentDelete());
    }

    public static function doCollaborateTask(TodoPayload $payload)
    {
        return (new Task())->doCollaborateTask($payload);
    }
}
