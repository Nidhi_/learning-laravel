<?php

namespace App\Todo\Task;

use Exception;
use App\Todo\TodoPayload;
use App\Todo\TodoException;
use App\Todo\TodoApi;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Todo\TodoValidation;
use App\Jobs\SendNotifications;

class Task extends Model
{
    use SoftDeletes;

    protected $table = 'tasks';
    protected $appends = ['is_task_owner'];

    /**
    * RELATION SHIPS
    */

    public function users()
    {
      return $this->belongsToMany('App\Todo\User\User', 'task_user');
    }

    public function media()
    {
      return $this->hasOne('App\Todo\Media\Media');
    }

    /**
    * SCOPES
    */

    // it will list all the incompleted task from the today's date
    public function scopeExpired($query) 
    {
      return $query->whereDate('completion_date','<',now());
    }

    // it will list all the expired task
    public function scopeGetExpiredTask($query) 
    {
      return $query->whereIsExpired(1);
    }

    // it will list all the expired task
    public function scopeGetExpiringTask($query) 
    {
      return $query->whereDate('completion_date', now());
    }

    /**
    * FUNCTIONS
    */

    // to check if the logged in user is owner of the task or not
    public function getIsTaskOwnerAttribute()
    {
      return TodoApi::getLoggedInUser()->id == $this->created_by;
    }

    public function getById($id) {
      return $this->findOrFail($id);
    }

    // Why guarded ? = Because we need all fields of this model in white list
    protected $guarded = [];

    public function getList(TodoPayload $payload) {
      return TodoApi::getLoggedInUser()->tasks()->paginate(5);
    }

    public function doCreate(TodoPayload $payload) {
      (new TaskValidation($payload))->validateCreate();
      $user = TodoApi::getLoggedInUser();
      // creating task
      $payload->created_by = $user->id;
      $task = $this->create((array) $payload);
      
      // also attaching task with the same user in pivot table
      $this->getById($task->id)
        ->users()
        ->attach($user->id);

      return $task;
    }

    public function getInstance(TodoPayload $payload, $id) {
      return $this->findOrFail($id);
    }

    public function doUpdate(TodoPayload $payload, $id) {
      (new TaskValidation($payload->task))->validateUpdate();
      return $this->getById($id)->update((array) $payload->task);
    }

    public function doDelete(TodoPayload $payload, $id) {
      $this->getById($id)->delete();
      //  return $this->users()->detach($id);
    }

    public function doDeleteMultiple(TodoPayload $payload) {
      // The destroy method can delete multiple records if we know ids
      return $this->destroy($payload->ids);
    }

    public function getCompletedTasks(TodoPayload $payload) {
      return TodoApi::getLoggedInUser()->tasks()->whereIsCompleted(1)->paginate(5);
    }

    public function getPendingTasks(TodoPayload $payload) {
      return TodoApi::getLoggedInUser()->tasks()->whereIsCompleted(0)->paginate(5);
    }

    public function getDeletedTasks(TodoPayload $payload) {
      return TodoApi::getLoggedInUser()->tasks()->onlyTrashed()->paginate(5);
    }

    public function doRestore(TodoPayload $payload, $id) {
      return $this->whereId($id)->restore();
    }

    public function doPermanentDelete(TodoPayload $payload) {
      return $this->whereId($payload->ids)->forceDelete();
    }

    public function doMarkExpire() {
      return $this->update(['is_expired' => 1]);
    }

    public function getExpiredTasks(TodoPayload $payload) {
      return TodoApi::getLoggedInUser()->tasks()->whereIsExpired(1)->paginate(5);
    }

    public function permanentDelete() {
      return $this->forceDelete();
    }

    public function doCollaborateTask(TodoPayload $payload) {
      // collaborating task with multiple users at a time
      foreach ($payload->users_id as $id) {
        
        $this->getById($payload->task_id)
          ->users()
          ->attach($id);
      }

      // will send notification mails via jobs
      SendNotifications::dispatch($payload->users_id)->onQueue('notification');
    }
}
