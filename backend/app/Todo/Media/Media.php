<?php

namespace App\Todo\Media;

use Exception;
use App\Todo\User\User;
use App\Todo\TodoPayload;
use App\Todo\TodoException;
use App\Todo\TodoValidation;
use App\Todo\TodoApi;
use App\Todo\Media\Media;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Media extends Model
{
    public $timestamps = false;

    protected $table = 'media';
    protected $guarded = [];

    /**
    * RELATION SHIPS
    */

    public function user()
    {
      return $this->hasOne('App\Todo\User\User');
    }


    public function doUpload(TodoPayload $payload, User $user) {
        $disk = Storage::disk('local');
        $temp_path = $this->createTempPath($payload, $user->id);
        $file_name = $this->createFileName($payload);
        if($user->media) {
            /**
             * 1. Delete the previous media from storage
             * 2. Save the new media with old name (So no need to update in DB)
             *
            **/
            $media_to_be_deleted = $temp_path.'/'.$user->media->name;
            if($disk->exists($media_to_be_deleted)) {
                $disk->delete($media_to_be_deleted);
            }
            $file_name = $user->media->name;
        }
        else {


            // save the file into db
            $media = Media::create([
                'user_id' => $user->id,
                'mime_type' => $payload->file->getClientOriginalExtension(),
                'name' => $file_name,
                'size' => $payload->file->getSize()
            ]);
        }
        // Save in storage
        $disk->putFileAs($temp_path, $payload->file, $file_name);

        return $media;
    }

    public function createTempPath($payload,$user_id) {
        $disk = Storage::disk('local');
        $root_path = '/images';
        // create a folder named 'images' if not exist
        if(!$disk->exists($root_path)) {
            $disk->makeDirectory($root_path);
        }

        //create user_id named folder inside 'images' if not exist. Ex- images/1 or images/2.
        if(!$disk->exists($root_path.'/'.$user_id)) {
            $disk->makeDirectory($root_path.'/'.$user_id);
        }

        // return temp path
        return $root_path.'/'.$user_id;
    }

    public function createFileName($payload) {
        return time().'.'.$payload->file->getClientOriginalExtension();
    }

    public function get(TodoPayload $payload, $id) {
        $media = $this->whereId($id)->findOrFail();
        $media_name = $media->name;
        $user_id = $media->user_id;
        $disk = Storage::disk('local');
        if ($disk->exists('images/'.$user_id.'/'.$media_name)){
            $file = $disk->get('images/'.$user_id.'/'.$media_name);
            $mimetype = $media->mime_type;
            $headers = [
                'Content-Type' => $mimetype
            ];
            return response($file, 200, $headers);
        }
    }
}
