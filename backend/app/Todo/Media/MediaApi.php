<?php

namespace App\Todo\Media;

use App\Todo\TodoApi;
use App\Todo\User\User;
use App\Todo\TodoPayload;

class MediaApi extends TodoApi
{
    public static function doUpload(TodoPayload $payload, User $user)
    {
        return (new Media())->doUpload($payload, $user);
    }

    public static function get(TodoPayload $payload, $id)
    {
        return (new Media())->get($payload, $id); 
    }
}
