<?php

namespace App\Todo\User;

use Exception;
use App\Todo\TodoPayload;
use App\Todo\TodoException;
use App\Todo\TodoApi;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Todo\TodoValidation;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Storage;
use App\Events\UserUpdated;
use App\Todo\Notification\NotificationApi;

class User extends Authenticatable
{
  use SoftDeletes;
  protected $table = 'users';
  protected $fillable = ['name','email','phone'];
  protected $appends = ['profile_image_url'];
  
  /**
   * RELATION SHIPS
   */


  public function tasks()
  {
    return $this->belongsToMany('App\Todo\Task\Task', 'task_user');
  }

  public function sessions()
  {
    return $this->hasMany('App\Todo\Session\Session');
  }

  public function media()
  {
    return $this->hasOne('App\Todo\Media\Media');
  }

  public function notifications()
  {
    return $this->hasMany('App\Todo\Notification\Notification');
  }
  
  /**
   * SCOPES
   */

   /**
    * FUNCTIONS
    */

    // To notify user if the profile has been updated
  public function notifyUser($user) {
    // FIXME:: remove hard coding of string
    $title = 'Profile has been updated';
    $type = 'Profile Updated';
    event(new UserUpdated($user, $title));
    
    $payload = new TodoPayload([
      'user_id' => $user->id,
      'type' => $type,
      'title' => $title
    ]);
    NotificationApi::doCreate($payload);
  }

  public function doRegister(TodoPayload $payload) {
     
    // validating user
    (new UserValidation($payload))->validateRegister();

    // registering user
    return $this->create((array) $payload); // ok check error
  }

  public function doUpdate(TodoPayload $payload, $id) {
    $user = $this->findOrFail($id);
    // update user
    $user->update((array) $payload);

    if($user->wasChanged('name')) {
      $this->notifyUser($user);
    }

    return $user;
  }

  public function getProfileImageUrlAttribute()
  {
    if ($this->media) {
      return route('get_media', ['id' => $this->media->id, 'timestamp' => now()->timestamp]);
    }
    return null;
  }

  public function getUserList() {
    return $this->get();
  }

  public function getCollaboratedTask() {
    $user = TodoApi::getLoggedInUser();
    return $user->tasks()->where('created_by', '!=' , $user->id)->paginate(5);
  }
}