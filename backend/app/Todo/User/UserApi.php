<?php

namespace App\Todo\User;

use App\Todo\TodoApi;
use App\Todo\TodoPayload;

class UserApi extends TodoApi
{
    static function findOrFailByEmail($email)
    {
        return User::whereEmail($email)->firstOrFail();
    }

    public static function doRegister(TodoPayload $payload)
    {
        return (new User())->doRegister($payload);
    }

    public static function doUpdate(TodoPayload $payload, $id)
    {
        return (new User())->doUpdate($payload, $id);
    }

    public static function getUserList()
    {
        return (new User())->getUserList();
    }

    public static function getCollaboratedTask()
    {
        return (new User())->getCollaboratedTask();
    }
}
