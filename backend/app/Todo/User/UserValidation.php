<?php

namespace App\Todo\User;

use App\Todo\TodoValidation;
use Illuminate\Foundation\Http\FormRequest;

class UserValidation extends TodoValidation
{
    const NAME = 'name';
    const EMAIL = 'email';
    const PHONE = 'phone';

    public function __construct($data)
    {
        $this->data = $data;

        $this->rules = [
            self::NAME => 'bail|required|string|min:3',
            self::EMAIL => 'bail|required|string|min:3',
            self::PHONE => 'bail|string',
        ];
    }

    public function validateRegister() {
        $fields = [
            self::NAME,
            self::EMAIL,
            self::PHONE
        ];

        $this->validate($fields, $this->data);
    }
}
