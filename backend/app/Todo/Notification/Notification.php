<?php

namespace App\Todo\Notification;

use Exception;
use App\Todo\TodoApi;
use App\Todo\TodoPayload;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $table = 'notifications';

    protected $fillable = ['type','title','user_id'];

    /**
    * RELATION SHIPS
    */

    public function user()
    {
      return $this->belongsTo('App\Todo\User\User');
    }

    public function doCreate(TodoPayload $payload) {
        $this->user_id = $payload->user_id;
        $this->type = $payload->type;
        $this->title = $payload->title;
        return $this->save();
    }

    public function getList(TodoPayload $payload) { 
        $user = TodoApi::getLoggedInUser();
        return $user->notifications()->latest()->get();
    }

    public function doMarkRead(TodoPayload $payload) {
        $user = TodoApi::getLoggedInUser();
        return $user->notifications()->whereIsRead(0)->update(['is_read' => 1]);
    }
}
