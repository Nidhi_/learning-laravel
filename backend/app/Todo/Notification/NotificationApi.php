<?php

namespace App\Todo\Notification;

use App\Todo\TodoApi;
use App\Todo\TodoPayload;

class NotificationApi extends TodoApi
{
    public static function doCreate(TodoPayload $payload)
    {
        return (new Notification())->doCreate($payload);
    }

    public static function getList(TodoPayload $payload)
    {
        return (new Notification())->getList($payload);
    }

    public static function doMarkRead(TodoPayload $payload)
    {
        return (new Notification())->doMarkRead($payload);
    }
}
