<?php

namespace App\Broadcasting;

use App\Todo\User\User;
use App\Todo\TodoApi;

class UserChannel
{
    /**
     * Create a new channel instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Authenticate the user's access to the channel.
     *
     * @param  \App\Todo\User\User  $user
     * @return array|bool
     */
    public function join(User $user)
    {
        return $user->id == TodoApi::getLoggedInUser()->id;
    }
}
