<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TodoCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:50',
            'description' => 'required',
            'deadline' => 'required'
        ];
    }

    /**
     * Get the rules messages.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'title.required' => 'The title field is required',
            'title.max' => 'Title should not be greater than 50 chars.',
            'description.required' => 'The description field is required',
            'deadline.required' => 'The deadline date is required'
        ];
    }
}
