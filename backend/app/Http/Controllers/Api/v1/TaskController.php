<?php

namespace App\Http\Controllers\Api\v1;

use App\Task;
use App\Todo\TodoPayload;
use App\Todo\Task\TaskApi;
use App\Todo\TodoException;
use Illuminate\Http\Request;
use App\Http\Validations\TaskValidation;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class TaskController extends Controller
{
    public function list(Request $request) {
        $result = ['message' => 'OK'];
        $payload = new TodoPayload($request->all());
        $result['result'] = TaskApi::getList($payload);
        return response()->format($result);
    }

    // Id of task to be get
    public function get(Request $request,$id) {
        $result = ['message' => 'OK'];
        $payload = new TodoPayload($request->all());
        $result['result'] = TaskApi::getInstance($payload, $id);
        return response()->format($result);
    }

    public function create(Request $request) {
        $result = ['message' => 'OK'];
        $payload = new TodoPayload($request->all());
        $result['result'] = TaskApi::doCreate($payload);
        return response()->format($result);
    }

    // Id of task to be update
    public function update(Request $request,$id) {
        $result = ['message' => 'OK'];
        $payload = new TodoPayload($request->all());
        $result['result'] = TaskApi::doUpdate($payload,$id);
        return response()->format($result);
    }

    // Id of task to be delete
    public function delete(Request $request,$id) {
        $result = ['message' => 'OK'];
        $payload = new TodoPayload($request->all());
        $result['result'] = TaskApi::doDelete($payload, $id);
        return response()->format($result);
    }

    // Delete multiple task
    public function deleteMultiple(Request $request) {
        $result = ['message' => 'OK'];
        $payload = new TodoPayload($request->all());
        $result['result'] = TaskApi::doDeleteMultiple($payload);
        return response()->format($result);
    }

    public function getCompletedTasks(Request $request) {
        $result = ['message' => 'OK'];
        $payload = new TodoPayload($request->all());
        $result['result'] = TaskApi::getCompletedTasks($payload);
        return response()->format($result);
    }

    public function getPendingTasks(Request $request) {
        $result = ['message' => 'OK'];
        $payload = new TodoPayload($request->all());
        $result['result'] = TaskApi::getPendingTasks($payload);
        return response()->format($result);
    }

    public function getDeletedTasks(Request $request) {
        $result = ['message' => 'OK'];
        $payload = new TodoPayload($request->all());
        $result['result'] = TaskApi::getDeletedTasks($payload);
        return response()->format($result);
    }

    // restore task
    public function restore(Request $request,$id) {
        $result = ['message' => 'OK'];
        $payload = new TodoPayload($request->all());
        $result['result'] = TaskApi::doRestore($payload, $id);
        return response()->format($result);
    }

    // permanent delete task
    public function permanentDelete(Request $request) {
        $result = ['message' => 'OK'];
        $payload = new TodoPayload($request->all());
        $result['result'] = TaskApi::doPermanentDelete($payload);
        return response()->format($result);
    }

    public function getExpiredTasks(Request $request) {
        $result = ['message' => 'OK'];
        $payload = new TodoPayload($request->all());
        $result['result'] = TaskApi::getExpiredTasks($payload);
        return response()->format($result);
    }

    public function collaborateTask(Request $request) {
        $result = ['message' => 'OK'];
        $payload = new TodoPayload($request->all());
        $result['result'] = TaskApi::doCollaborateTask($payload);
        return response()->format($result);
    }
}
