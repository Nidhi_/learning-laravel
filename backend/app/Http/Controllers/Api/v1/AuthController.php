<?php

namespace App\Http\Controllers\Api\v1;

use App\Todo\TodoPayload;
use App\Todo\User\UserApi;
use App\Todo\User\User;
use App\Todo\Session\SessionApi;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    public function register(Request $request) {
        $result = ['message' => 'OK'];
        $payload = new TodoPayload($request->all());

        // Doing registration of user
        $user = UserApi::doRegister($payload);

        // Getting Ip address of user
        $payload->ip_address = $request->getClientIp();

        // Creating session of registered user
        $session = SessionApi::create($payload, $user);
        $result['result'] = $session->key;
        return response()->format($result);
    }

    public function verifyOtp(Request $request) {
        $result = ['message' => 'OK'];
        $payload = new TodoPayload($request->all());

        // let's verify the user
        $session = SessionApi::verifyOtp($payload->key, $payload);
        $result['result'] = $session->secret;

        // get user from session user_id
        $user = User::whereId($session->user_id)->firstOrFail();
        $result['user'] = $user;
        
        return response()->format($result);
    }

    public function login(Request $request)
    {
        $result = ['message' => 'OK'];
        $payload = new TodoPayload($request->all());
        $payload->ip_address = $request->getClientIp();

        // Check whether user exists or not
        $user = User::whereEmail($payload->email)->firstOrFail();
        
        // Creating session of user
        $session = SessionApi::create($payload, $user);
        $result['result'] = $session->key;
        return response()->format($result);
    }

    public function logout(Request $request)
    {
        $result = ['message' => 'OK'];
        $payload = new TodoPayload($request->all());

        //let him logout
        $session = SessionApi::logout($payload->key);
        return response()->format($result);
    } 
}
