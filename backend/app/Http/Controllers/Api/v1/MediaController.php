<?php

namespace App\Http\Controllers\Api\v1;
use App\Todo\TodoPayload;
use App\Todo\Media\Media;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class MediaController extends Controller
{
    public function get(Request $request, $media_id) {
        $result = ['message' => 'OK'];
        $payload = new TodoPayload($request->all());
        $media = Media::findOrFail($media_id);

        $disk = Storage::disk('local');
        $media_path = 'images/'.$media->user_id.'/'.$media->name;

        if ($disk->exists($media_path)){
            $file = $disk->get($media_path);
            $headers = [
                'Content-Type' => $media->mime_type
            ];
            return response($file, 200, $headers);
        }
    }
}
