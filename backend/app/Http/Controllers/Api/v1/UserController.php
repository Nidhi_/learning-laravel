<?php

namespace App\Http\Controllers\Api\v1;

use App\Todo\User\User;
use App\Todo\TodoPayload;
use App\Todo\User\UserApi;
use App\Todo\TodoException;
use App\Todo\Media\MediaApi;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Todo\TodoApi;

class UserController extends Controller
{
    // Update user name
    public function update(Request $request, $id) {
        $result = ['message' => 'OK'];
        $payload = new TodoPayload($request->all());
        $result['result'] = UserApi::doUpdate($payload, $id);
        return response()->format($result);
    }

    public function uploadProfileImage(Request $request, User $user) {
        $result = ['message' => 'OK'];
        $payload = new TodoPayload($request->all());

        $media = MediaApi::doUpload($payload, $user);

        $result['result'] = $media;
        return response()->format($result);
    }

    public function get(Request $request) {
        $result = ['message' => 'OK'];
        $payload = new TodoPayload($request->all());

        $result['result'] = TodoApi::getLoggedInUser();
        return response()->format($result);
    }

    public function list(Request $request) {
        $result = ['message' => 'OK'];
        $payload = new TodoPayload($request->all());

        $result['result'] = UserApi::getUserList();
        return response()->format($result);
    }

    public function getCollaboratedTask(Request $request) {
        $result = ['message' => 'OK'];
        $payload = new TodoPayload($request->all());
        $result['result'] = UserApi::getCollaboratedTask();
        return response()->format($result);
    }
}
