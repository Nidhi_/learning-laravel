<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Todo\TodoPayload;
use App\Todo\Notification\NotificationApi;
use App\Http\Controllers\Controller;

class NotificationController extends Controller
{
    public function list(Request $request) {
        $result = ['message' => 'OK'];
        $payload = new TodoPayload($request->all());
        $result['result'] = NotificationApi::getList($payload);
        return response()->format($result);
    }

    public function markRead(Request $request) {
        $result = ['message' => 'OK'];
        $payload = new TodoPayload($request->all());
        $result['result'] = NotificationApi::doMarkRead($payload);
        return response()->format($result);
    }
}
