<?php

namespace App\Providers;

use Illuminate\Support\Facades\Broadcast;
use Illuminate\Support\ServiceProvider;

class BroadcastServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // It will apply middleware to all the routes that are called from frontend through broadcasting
        Broadcast::routes([
            'middleware' => 'auth:api'
        ]);

        require base_path('routes/channels.php');
    }
}
