<?php

namespace App\Providers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Response;

class ResponseMacroServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Response::macro('format', function($data)
        {
            $error = null;

            if ($data instanceof \Exception) {
                $e = $data;

                $error = new \stdClass();
                $error->title = $e->title ?? 'Error !';
                $error->message = $e->getMessage();
                $error->code = $e->getCode();
            }

            $formatted_response = [
                'data' => $error ? [] : $data,
                'error' => $error ?? null
            ];
            return Response::make($formatted_response);
        });
    }
}
