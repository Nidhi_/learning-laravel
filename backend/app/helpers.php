<?php

use Illuminate\Support\Facades\App;

/**
 * Hash legitimate value
 */
function doHash($input)
{
    return !empty($input) ? hash('sha512', $input) : null;
}

function generateOtp()
{
    if (App::environment(['local', 'test', 'stage', 'testing'])) {
        return 123456;
    } else {
        return rand(100000, 999999);
    }
}