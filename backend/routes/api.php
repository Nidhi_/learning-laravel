<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['prefix' => '/v1', 'namespace' => 'Api\v1'], function () {
    
    Route::group([
        'prefix' => 'auth'
        ], function () {
        Route::post('/register','AuthController@register');
        Route::post('/verifyOtp','AuthController@verifyOtp');
        Route::post('/login','AuthController@login');
        Route::post('/logout','AuthController@logout');
    });

    Route::group([
        'prefix' => 'user',
        'middleware' => ['auth:api']
        ], function () {
        Route::post('/update/{id}','UserController@update');
        Route::post('/{user}/image_upload','UserController@uploadProfileImage');
        Route::post('/get/{id}','UserController@get');
        Route::post('/notifications','NotificationController@list'); 
        Route::post('/notifications/mark_read','NotificationController@markRead');
        Route::post('/list','UserController@list');
        Route::post('/collaborated_task','UserController@getCollaboratedTask');
    });

    Route::group([
        'prefix' => 'task',
        'middleware' => ['auth:api']
        ], function () {
        Route::post('/','TaskController@list');
        Route::post('/{id}','TaskController@get');
        Route::post('/create','TaskController@create');
        Route::post('/update/{id}','TaskController@update');
        Route::post('/delete/{id}','TaskController@delete');
        Route::post('/delete/multiple','TaskController@deleteMultiple');
        Route::post('/completed_task','TaskController@getCompletedTasks');
        Route::post('/pending_task','TaskController@getPendingTasks');
        Route::post('/deleted_task','TaskController@getDeletedTasks');
        Route::post('/restore/{id}','TaskController@restore');
        Route::post('/permanent_delete','TaskController@permanentDelete');
        Route::post('/expired_task','TaskController@getExpiredTasks');
        Route::post('/collaborate_task','TaskController@collaborateTask');
    });

    Route::get(
        '/media/{id}/{timestamp}',
        'MediaController@get'
    )->name('get_media');
});
