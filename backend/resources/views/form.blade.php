@extends("layouts.app")
@section("content")
<div class="row">
  <div class="col-md-12">
    <h1> Create a New Task ! </h1>
    <div style="text-align: center;">
      @if ($errors->any())
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
          @foreach ($errors->all() as $error)
            {{ $error }}
          @endforeach
        </div>
      @endif
    </div>
    <form method="post" action="/create">
      @csrf
      <fieldset>
        <label>Title:</label>
        <input type="text" id="title" name="title">
      
        <label>Description:</label>
        <textarea name="description" placeholder="Write description about your task"></textarea>
     
        <label>Completion Date:</label>
        <input type="date" id="completion_date" name="completion_date">
        
      </fieldset>       
      <button type="submit">Create</button>
      
    </form>
  </div>
</div>
@endsection