@extends("layouts.app")
@section("content")
<div class="row">
  <div class="col-md-12">
    <h1> {{ $task->title }} </h1>
    <form>
      @csrf
      <fieldset>
        <p><b>Description:</b>{{ $task->description }}</p>
     
        <p><b>Completion Date:</b> {{ $task->completion_date }}</p>

        <p><b>Is Completed:</b> {{ $task->is_completed }}</p></label>
                
      </fieldset>
      <a href="/" class="list-btn rounded text-white">Back</a>
    </form>
  </div>
</div>
@endsection