@extends("layouts.app")
@section("content")
<div class="row">
  <div class="col-md-12">
    <h1> Our task list ! <a href="/form/create" class="list-btn text-white rounded">Create new</a></h1>

    <div style="text-align: center;">
      @if(session()->has('message'))
      <div class="alert alert-success alert-dismissible fade show" role="alert">
        <strong>{{ session()->get('message') }}</strong>
      </div>
      @endif
    </div>

    <div class="list">
      <table style="width:100%">
        @foreach ($tasks as $task)
        <tr>
          <th style="width:40%"><div>{{ $task->title }}</div></th>
          <th><a href="/{{ $task->id }}" class="list-btn text-white rounded">View</a></th> 
          <th><a href="/form/edit/{{ $task->id }}" class="list-btn text-white rounded">Edit</a></th>
          <td>
            <form action="/delete/{{ $task->id }}" method="POST">
              {!! csrf_field() !!}
              {!! method_field('DELETE') !!}
              <button class="list-btn rounded" style="width:50%;">Delete</button>
            </form>
          </td>
        </tr>
        @endforeach
      </table>
                
      </div>
  </div>
</div>
@endsection
