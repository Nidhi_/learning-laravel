@extends("layouts.app")
@section("content")
  <div class="container">
  @if(session()->has('message'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
      <strong>Done !!! </strong>{{ session()->get('message') }}
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  @endif
    <div class="col-md-12">
      <h1>What we need to do?</h1>
      @if ($errors->any())
        <div color="yellow">
          <ul>
          @foreach ($errors->all() as $error)
             <li>{{ $error }}</li>
          @endforeach
          </ul>
        </div>
      @endif

      <div class="col-md-6">
        <form method="post" action="/todos/create" class="py-5">
        @csrf
          <label>Title</label>
          <input type="text" name="title" />
  
          <textarea name="description" placeholder="Write something about your task" style="height:200px; width: 300px;"></textarea>
  
          <div class="col-md-12">
            <label>Deadline</label>
            <input type="date" name="deadline" />

            <input type="submit" value="create">
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection