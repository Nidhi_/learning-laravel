@extends("layouts.app")
@section("content")
<div class="row">
  <div class="col-md-12">
    <h1> Edit Task !! </h1>
    <div style="text-align: center;">
      @if ($errors->any())
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
          @foreach ($errors->all() as $error)
            {{ $error }}
          @endforeach
        </div>
      @endif
    </div>
    <form method="post" action="/update/{{ $task->id }}">
        @csrf
      <fieldset>
        <label>Title:</label>
        <input type="text" name="title" value="{{ $task->title }}"></input>
      
        <label>Description:</label>
        <textarea name="description" value="{{ $task->description }}"></textarea>
     
        <label>Completion Date:</label>
        <input type="date" value="{{ $task->completion_date }}" name="completion_date">
        
      </fieldset>       
      <button type="submit">Update</button>
      <a href="/" class="list-btn rounded">Back</a>
    </form>
    
  </div>
</div>
@endsection