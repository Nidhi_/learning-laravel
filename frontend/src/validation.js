import i18n from "./i18n"
export default {
  required: value => !!value || i18n.t ('validation.required'),

  exact_length: length => {
    return function (value) {
      if (value && value.length != length) {
        return i18n.t ('validation.exact_length', {length: length});
      }
      return true;
    };
  },

  min_length: length => {
    return function (value) {
      if (value && value.length < length) {
        return i18n.t ('validation.min_length', {length: length});
      }
      return true;
    };
  },

  max_length: length => {
    return function (value) {
      if (value && value.length > length) {
        return i18n.t ('validation.max_length', {length: length});
      }
      return true;
    };
  },

  email: value => {
      const pattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      if(value){
        return pattern.test(value) || i18n.t ('validation.email');
      }
      return true
  },

  numeric: value => {
    const pattern = /^\d+$/
    return pattern.test(value) || i18n.t ('validation.numeric')
  }
}
