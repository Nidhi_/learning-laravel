import DEFINES from '@/defines'
import Pusher from 'pusher-js';
import {buildHeaders} from '@/api/_manager';
import Vue from 'vue'
import { EventBus } from "@/event-bus";

function initialState () {
}

const state = initialState ();

const actions = {
  // setup of notifications
  async setupPusherForNotifications (context) {
    let pusher = await context.dispatch('initiatePusher');
    let channel = await context.dispatch('subscribeToChannel', {pusher:pusher});
    return await context.dispatch('bindChannelToListenEvent', {channel:channel});
  },

  // To authenticate user
  async initiatePusher() {
    let pusher = await new Pusher(DEFINES.PUSHER_APP_KEY, {
      cluster: DEFINES.PUSHER_APP_CLUSTER,
      authEndpoint: `${DEFINES.SERVER_URL}/broadcasting/auth`,
      auth: {
        headers: buildHeaders().headers,
      }
    });
    return pusher;
  },

  // To subscribe the channel
  async subscribeToChannel(context, payload) {
    let channel = await payload.pusher.subscribe (`private-todolist-development.${context.rootState.user.user.id}`);
    return channel;
  },

  // To listen the event
  async bindChannelToListenEvent(context, payload) {

    // FIXME: USE ANOTHER METHOD
    var events = DEFINES.PUSHER_EVENTS;
    for( var i = 0; i < events.length;i++ ) {
      payload.channel.bind( events[i], function(data) {
        Vue.notify({
          group: 'notification',
          text: data.title,
          type: 'success',
          duration: 1000,
          speed: 1000
        });
        EventBus.$emit('notification_recieved');
      });
    }
  }
};

export default {
  namespaced: true,
  state,
  actions,
};