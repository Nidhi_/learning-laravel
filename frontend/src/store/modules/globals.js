import {EventBus} from '@/event-bus.js'
import DEFINES from '@/defines'

function initialState () {
  return {
    layout: null
  }
}

// state
const state = initialState ()

// getters
const getters = {
  currentLayout: state => state.layout,

  isLoggedIn: (state, getters, rootState) => {
    return rootState.auth.secret
  },
}

// actions
const actions = {
  showMessage (context, payload) {
    EventBus.$emit('showMessage', payload)
  },

  async setAppLayout (context, payload) {
    await context.commit ('setAppLayout', payload)
  },

  async reset (context) {
    context.commit (
      'reset', DEFINES.USE_ROOT
    )
  }
}

// mutations
const mutations = {
  setAppLayout (state, payload) {
    state.layout = payload.layout;
  },

  reset (state) {
    // acquire initial state
    const s = initialState ();
    Object.keys (s).forEach (key => {
      state[key] = s[key];
    });
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
