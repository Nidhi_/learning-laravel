import * as API from '../../api/auth';
import DEFINES from '@/defines'
import router from '../../router';

function initialState () {
  return {
    key: null,
    secret: null
  };
}

const state = initialState ();

const actions = {
  async register (context, payload) {
    let response = await API.Auth.register (payload);
    context.commit ('set_key', {
      key: response.result,
    });
    return response;
  },

  async verifyOtp (context, payload) {
    let response = await API.Auth.verifyOtp (payload);
    context.commit ('set_secret', {
      secret: response.result,
    });
    context.dispatch (
      'user/setUser', 
      {
        user: response.user
      },
      DEFINES.USE_ROOT
    )
    return response;
  },

  async login (context, payload) {
    let response = await API.Auth.login (payload);
    context.commit ('set_key', {
      key: response.result,
    });
    return response;
  },

  async logout (context, payload) {
    let response = await API.Auth.logout (payload);
    context.dispatch ('resetAll', {}, DEFINES.USE_ROOT);
    router.push ({path: '/login'});
    return response;
  },

  async reset (context) {
    context.commit (
      'reset', DEFINES.USE_ROOT
    )
  }
}

const getters = {
  getKey: state => state.key
};

const mutations = {
  set_key (state, payload) {
    state.key = payload.key;
  },

  set_secret (state, payload) {
    state.secret = payload.secret;
  },

  reset (state) {
    // acquire initial state
    const s = initialState ();
    Object.keys (s).forEach (key => {
      state[key] = s[key];
    });
  }
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters,
};