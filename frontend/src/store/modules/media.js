import * as API from '../../api/media';

function initialState () {
  return {
    image: null
  };
}

const state = initialState ();

const actions = {
  async uploadImage (context, payload) {
    let response = await API.Media.uploadImage (payload);
    return response;
  }
}

const getters = {
};

const mutations = {
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters,
};