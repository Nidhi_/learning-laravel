import * as API from '../../api/task';
import DEFINES from '@/defines'

function initialState () {
  return {
    list: [],
    completed_task_list: [],
    pending_task_list: [],
    deleted_task_list: [],
    expired_task_list: []
  };
}

const state = initialState ();

const actions = {
  async create (context, payload) {
    let response = await API.Task.create (payload);
    return response;
  },

  async update (context, payload) {
    let response = await API.Task.update (payload);
    return response;
  },

  async list (context, payload) {
    let response = await API.Task.list (payload);
    context.commit ('set_list', {
      list: response.result.data,
    });
    return response;
  },
    
  async delete (context, payload) {
    let response = await API.Task.delete (payload);
    return response;
  },

  async get (context, payload) {
    let response = await API.Task.get (payload);
    return response.result;
  },

  async deleteMultiple(context,payload) {
    let response = await API.Task.deleteMultiple (payload);
    return response;
  },

  async reset (context) {
    context.commit (
      'reset', DEFINES.USE_ROOT
    )
  },

  async getCompletedTasks (context, payload) {
    let response = await API.Task.completedTaskList (payload);
    context.commit ('set_completed_task_list', {
      completed_task_list: response.result.data,
    });
    return response;
  },

  async getPendingTasks (context, payload) {
    let response = await API.Task.pendingTaskList (payload);
    context.commit ('set_pending_task_list', {
      pending_task_list: response.result.data,
    });
    return response;
  },

  async getDeletedTasks (context, payload) {
    let response = await API.Task.deletedTaskList (payload);
    context.commit ('set_deleted_task_list', {
      deleted_task_list: response.result.data,
    });
    return response;
  },

  async getExpiredTasks (context, payload) {
    let response = await API.Task.expiredTaskList (payload);
    context.commit ('set_expired_task_list', {
      expired_task_list: response.result.data,
    });
    return response;
  },

  async restore (context, payload) {
    let response = await API.Task.restore (payload);
    return response;
  },

  async permanentDeleteMultiple(context,payload) {
    let response = await API.Task.permanentDelete (payload);
    return response;
  },

  async collaborateTask(context,payload) {
    let response = await API.Task.collaborateTask (payload);
    return response;
  }
};

const getters = {};

const mutations = {
  set_list (state, payload) {
    state.list = payload.list;
  },

  reset (state) {
    // acquire initial state
    const s = initialState ();
    Object.keys (s).forEach (key => {
      state[key] = s[key];
    });
  },

  set_completed_task_list (state, payload) {
    state.completed_task_list = payload.completed_task_list;
  },

  set_pending_task_list (state, payload) {
    state.pending_task_list = payload.pending_task_list;
  },

  set_deleted_task_list (state, payload) {
    state.deleted_task_list = payload.deleted_task_list;
  },

  set_expired_task_list (state, payload) {
    state.expired_task_list = payload.expired_task_list;
  }
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters,
};
