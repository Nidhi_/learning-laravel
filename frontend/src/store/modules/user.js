import * as API from '../../api/user';
import DEFINES from '@/defines'

function initialState () {
  return {
    // FIXME:: check if global state variables work on null
    user: {},
    list: {},
    task: {}
  };
}

const state = initialState ();

const actions = {
  async update (context, payload) {
    let response = await API.User.update (payload);
    context.commit (
      'set_user',
      {
        user: response.result
      }
    )
    return response;
  },

  async setUser (context, payload){
    context.commit (
      'set_user',
      {
        user: payload.user
      }
    )
  },

  async reset (context) {
    context.commit (
      'reset', DEFINES.USE_ROOT
    )
  },

  async get (context, payload) {
    let response = await API.User.get (payload);
    context.commit (
      'set_user',
      {
        user: response.result
      }
    )
    return response;
  },

  async list (context, payload) {
    let response = await API.User.list (payload);
    context.commit (
      'set_list',
      {
        list: response.result
      }
    )
    return response;
  },

  async getCollaboratedTask(context,payload) {
    let response = await API.User.getCollaboratedTask (payload);
    context.commit ('set_task', {
      task: response.result.data,
    });
    return response;
  }
}

const getters = {
};

const mutations = {
  set_user (state, payload) {
    state.user = payload.user;
  },

  set_list (state, payload) {
    state.list = payload.list;
  },

  set_task (state, payload) {
    state.task = payload.task;
  },

  reset (state) {
    // acquire initial state
    const s = initialState ();
    Object.keys (s).forEach (key => {
      state[key] = s[key];
    });
  }
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters,
};