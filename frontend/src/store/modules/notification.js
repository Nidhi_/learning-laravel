import * as API from '../../api/notification';

function initialState () {
  return {
    list: []
  };
}

const state = initialState ();

const actions = {
  async list (context, payload) {
    let response = await API.Notification.list (payload);
    context.commit ('set_list', {
      list: response.result,
    });
    return response;
  },

  async markAsRead(context, payload) {
    let response = await API.Notification.markAsRead (payload);
    return response;
  }
};

const getters = {};

const mutations = {
  set_list (state, payload) {
    state.list = payload.list;
  }
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters,
};
