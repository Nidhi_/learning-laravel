import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate';

import task from './modules/task'
import globals from './modules/globals'
import auth from './modules/auth'
import user from './modules/user'
import media from './modules/media'
import event from './modules/event'
import notification from './modules/notification'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
    resetAll({commit}) {
      commit ('globals/reset');
      commit ('auth/reset');
      commit ('task/reset');
    },
  },
  modules: {
    namespaced: true,
    task,
    globals,
    auth,
    user,
    media,
    event,
    notification
  },
  
  plugins: [
    createPersistedState ({
      key: 'task',
    }),
  ],
})
