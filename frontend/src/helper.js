const isExistInArray = (arr, item) => {
  return arr.includes (item);
};

const pushIntoArray = (arr, item) => {
  arr.push (item);
  return arr;
};

const removeFromArray = (arr, item_to_be_removed) => {
  return arr.filter (function (item) {
    return item != item_to_be_removed;
  });
};

export {isExistInArray, pushIntoArray, removeFromArray};
