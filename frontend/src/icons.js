export default {
  td_delete: 'mdi-delete',
  td_add: 'mdi-plus',
  td_close: 'mdi-close',
  td_task_selected: 'mdi-checkbox-marked',
  td_task_unselected: 'mdi-checkbox-blank-outline',
  td_back: 'mdi-arrow-left',
  td_view_list: 'mdi-chevron-right',
  td_search:'mdi-magnify',
  td_dot_menu:'mdi-dots-vertical',
  td_logout: 'mdi-power',
  td_account: 'mdi-account-circle',
  td_edit: 'mdi-pencil',
  td_pending: 'mdi-clock',
  td_check: 'mdi-check',
  td_right: 'mdi-chevron-right',
  td_user: 'mdi-account',
  td_mail: 'mdi-email',
  td_phone: 'mdi-phone',
  td_otp: 'mdi-account-key',
  td_camera: 'mdi-camera',
  td_restore: 'mdi-restore',
  td_nothing_emoji: 'mdi-emoticon-sad-outline',
  td_expired: 'mdi-exclamation',
  td_notification: 'mdi-bell',
  td_collaborate : 'mdi-call-merge'
};
