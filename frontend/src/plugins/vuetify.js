import Vue from 'vue';
import Vuetify from 'vuetify/lib';

// Import custom icon files
import APP_CUSTOM_ICONS from '@/icons';

Vue.use(Vuetify);

export default new Vuetify({
  iconfont: 'mdi', // default - only for display purposes
  icons: {
    values: APP_CUSTOM_ICONS,
  },
});
