export default {
    BASE_API_URL: process.env.VUE_APP_API_URL,
    SERVER_URL: process.env.VUE_APP_SERVER_URL,
    PUSHER_APP_KEY: process.env.VUE_APP_PUSHER_APP_KEY,
    PUSHER_APP_CLUSTER: process.env.VUE_APP_PUSHER_APP_CLUSTER,

    USE_ROOT: {root: true},
    
    LAYOUT_AUTH: 'LAYOUT_AUTH',
    LAYOUT_APP: 'LAYOUT_APP',

    PUSH_OPERATION_STRING: 'push',
    REMOVE_OPERATION_STRING: 'remove' ,

    PUSHER_EVENTS: ['user.updated','task.deleted','task.expired']
}