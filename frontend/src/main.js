import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';
import VueI18n from 'vue-i18n'
import i18n from './i18n'
import DEFINES from './defines';
import moment from 'moment';
import Notifications from 'vue-notification'

Vue.use(VueI18n)
Vue.config.productionTip = false
Vue.prototype.DEFINES = DEFINES
Vue.prototype.moment = moment
Vue.use(Notifications)

new Vue({
  router,
  store,
  vuetify,
  i18n,
  render: h => h(App)
}).$mount('#app')
