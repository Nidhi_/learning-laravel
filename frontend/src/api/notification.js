import * as ApiManager from './_manager';

export const Notification = {
  list: payload => {
    return ApiManager.callApi ('/user/notifications', payload);
  },
  
  markAsRead: payload => {
    return ApiManager.callApi ('/user/notifications/mark_read', payload);
  }
};
