import * as ApiManager from './_manager';

export const Auth = {
  register: payload => {
    return ApiManager.callApi ('/auth/register', payload);
  },

  verifyOtp: payload => {
    return ApiManager.callApi ('/auth/verifyOtp', payload);
  },

  login: payload => {
    return ApiManager.callApi ('/auth/login', payload);
  },

  logout: payload => {
    return ApiManager.callApi ('/auth/logout', payload);
  }
};
