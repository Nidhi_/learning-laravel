import * as ApiManager from './_manager';

export const Task = {
  list: payload => {
    return ApiManager.callApi ('/task/', payload);
  },

  create: payload => {
    return ApiManager.callApi ('/task/create', payload);
  },

  delete: payload => {
    return ApiManager.callApi (`/task/delete/${payload.id}`, payload);
  },

  get: payload => {
    return ApiManager.callApi (`/task/${payload.id}`, payload);
  },

  update: payload => {
    return ApiManager.callApi (`/task/update/${payload.task.id}`, payload);
  },

  deleteMultiple: payload => {
    return ApiManager.callApi (`/task/delete/multiple`, payload);
  },

  completedTaskList: payload => {
    return ApiManager.callApi (`/task/completed_task`, payload);
  },

  pendingTaskList: payload => {
    return ApiManager.callApi (`/task/pending_task`, payload);
  },

  deletedTaskList: payload => {
    return ApiManager.callApi (`/task/deleted_task`, payload);
  },

  restore: payload => {
    return ApiManager.callApi (`/task/restore/${payload.id}`, payload);
  },

  permanentDelete: payload => {
    return ApiManager.callApi (`/task/permanent_delete`, payload);
  },

  expiredTaskList: payload => {
    return ApiManager.callApi (`/task/expired_task`, payload);
  },

  collaborateTask: payload => {
    return ApiManager.callApi (`/task/collaborate_task`, payload);
  }
};
