import axios from 'axios';
import store from '../store';
// import router from '@/router';

const URL = process.env.VUE_APP_API_URL;

const parseResponse = api_response => {
  let response = api_response.data;
  
  // If response error code is 419
  if(response.error && response.error.code == 419) {
    store.dispatch("auth/logout", {
      key: store.state.auth.key
    });
  }

  // If error is set on response
  if (response.error) {
    throw response.error;
  }
  
  // If data is set and status is 200
  if (api_response.status == 200) {
    return response.data;
  }
};


async function callApi (namespace, payload) {
  return axios.post(URL + namespace, payload, buildHeaders())
    .then (response => {
      return parseResponse (response);
    })
    .catch (function (error) {
      throw error;
    });
}

const buildHeaders = () => {
  return {
    headers: {
      Authorization : 'Bearer ' + getTokenHash()
    },
  };
}

const getTokenHash = () => {
  let nonce = require('nonce')();
  let timestamp = '1' + nonce();

  let key = store.state.auth.key;
  let secret = store.state.auth.secret;
  let hash = null;

  if (key && secret) {
    hash = doHash(key + secret + timestamp);
  }

  let payload = {
    key : key,
    timestamp : timestamp,
    hash : hash
  };
  return JSON.stringify(payload);
}

const doHash = (to_be_hashed) => {
  let shajs = require('sha.js');
  return shajs('sha256').update(to_be_hashed).digest('hex');
}

export {URL, axios, callApi, buildHeaders};
