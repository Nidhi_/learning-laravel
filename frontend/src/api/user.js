import * as ApiManager from './_manager';

export const User = {
  update: payload => {
    return ApiManager.callApi (`/user/update/${payload.id}`, payload);
  },

  get: payload => {
    return ApiManager.callApi (`/user/get/${payload.id}`, payload);
  },

  list: payload => {
    return ApiManager.callApi (`/user/list`, payload);
  },

  getCollaboratedTask: payload => {
    return ApiManager.callApi (`/user/collaborated_task`, payload);
  }
};
