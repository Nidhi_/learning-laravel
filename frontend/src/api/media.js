import * as ApiManager from './_manager';

export const Media = {
  uploadImage: payload => {
    return ApiManager.callApi (`/user/${payload.id}/image_upload`, payload);
  }
};
