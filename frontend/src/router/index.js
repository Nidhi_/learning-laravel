import Vue from 'vue'
import VueRouter from 'vue-router'
import paths from './paths'
import store from '../store'
import DEFINES from '../defines'

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  base: '/',
  linkActiveClass: 'active',
  routes: paths
})

router.beforeEach ((to, from, next) => {  
  var oldLayout = store.getters['globals/appLayout'];
  var newLayout = to.meta.layout || DEFINES.LAYOUT_APP;

  if (newLayout != oldLayout) {
    store.dispatch (
      'globals/setAppLayout',
      { layout: newLayout }
    );
  }

  // If already logged in, it should redirect to dashboard
  if (store.getters['globals/isLoggedIn'] && to.path.startsWith ('/login')) {
    return next ({name: 'Dashboard'});
  }
  
  // if not logged in, can't access public pages
  if ( to.matched.some (record => record.meta.public) != true &&
    !store.getters['globals/isLoggedIn']
  ) {
    return next ({name: 'Login'});
  }
  next ()
})

export default router
