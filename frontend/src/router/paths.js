import DEFINES from '../defines'

import Dashboard from '../pages/Dashboard.vue';
import TaskOverview from '../pages/Task/Overview';
import Register from '../pages/Auth/Register';
import Login from '../pages/Auth/Login';
import Profile from '../pages/Profile.vue';
import TaskListPage from '../pages/Task/List.vue'
import Notification from '../pages/Notifications.vue'

export default [
  {
    path: '/',
    name: 'Dashboard',
    component: Dashboard,
    meta: {
      layout: DEFINES.LAYOUT_APP,
      title: 'Task List'
    }
  },
  {
    path: '/task/:id',
    name: 'TaskOverview',
    component: TaskOverview,
    meta: {
      layout: DEFINES.LAYOUT_APP,
      title: 'Task'
    }
  },
  {
    path: '/register',
    name: 'Register',
    component: Register,
    meta: {
      public: true,
      layout: DEFINES.LAYOUT_AUTH
    }
  },
  {
    path: '/login',
    name: 'Login',
    component: Login,
    meta: {
      public: true,
      layout: DEFINES.LAYOUT_AUTH
    }
  },
  {
    path: '/user/:user_id/profile',
    name: 'Profile',
    component: Profile,
    meta: {
      backRoute: 'Dashboard',
      layout: DEFINES.LAYOUT_APP,
      title: 'Profile'
    }
  },
  {
    path: '/user/:user_id/completed-task',
    name: 'CompletedTask',
    component: TaskListPage,
    meta: {
      backRoute: 'Profile',
      layout: DEFINES.LAYOUT_APP,
      title: 'Completed Task'
    }
  },
  {
    path: '/user/:user_id/deleted-task',
    name: 'DeletedTask',
    component: TaskListPage,
    meta: {
      backRoute: 'Profile',
      layout: DEFINES.LAYOUT_APP,
      title: 'Deleted Task'
    }
  },
  {
    path: '/user/:user_id/pending-task',
    name: 'PendingTask',
    component: TaskListPage,
    meta: {
      backRoute: 'Profile',
      layout: DEFINES.LAYOUT_APP,
      title: 'Pending Task'
    }
  },
  {
    path: '/user/:user_id/expired-task',
    name: 'ExpiredTask',
    component: TaskListPage,
    meta: {
      backRoute: 'Profile',
      layout: DEFINES.LAYOUT_APP,
      title: 'Expired Task'
    }
  },
  {
    path: '/notification',
    name: 'Notification',
    component: Notification,
    meta: {
      backRoute: 'Dashboard',
      layout: DEFINES.LAYOUT_APP,
      title: 'Notifications'
    }
  },
  {
    path: '/user/:user_id/collaborated-task',
    name: 'CollaboratedTask',
    component: TaskListPage,
    meta: {
      backRoute: 'Dashboard',
      layout: DEFINES.LAYOUT_APP,
      title: 'Collaborated Task'
    }
  }
];
